//
//  OrdersTableViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class OrdersTableViewController : UITableViewController {
    
    var orders : [[String: AnyObject]] = []
    let cartService = CartService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartService.delegate = self
        cartService.requestOrders()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)
        cell.textLabel?.text = orders[indexPath.item]["name"] as? String ?? "Default Address"
        cell.detailTextLabel?.text = orders[indexPath.item]["address"] as? String ?? "Default Address"
        return cell
    }
}

extension OrdersTableViewController : CartServiceProtocol {
    func didCreateOrder() {
        //
    }
    
    func didReceivedOrders(data: [[String : AnyObject]]?) {
        print(data)
    }
}
