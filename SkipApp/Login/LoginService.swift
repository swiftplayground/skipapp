//
//  LoginService.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

protocol LoginServiceProtocol : class {
    func didLoggedIn()
    func didLoggedOut()
    func didLoginUnauthorized(code: Int)
}

class LoginService {
    
    weak var delegate : LoginServiceProtocol?
    
    func requestLogin(payload: [(String,String)]){
        GenericRestClient().login(payload: payload, completion: { data in
            
            if let status = data as? Int {
                self.delegate?.didLoginUnauthorized(code: 0)
                return
            }
            
            if let token = String(data: data as! Data, encoding: String.Encoding.utf8) {
                let userDefaults = UserDefaults.standard
                if userDefaults.object(forKey: "jwt_token") == nil {
                    userDefaults.set(token, forKey: "jwt_token")
                    userDefaults.synchronize()
                }
                self.delegate?.didLoggedIn()
            }
        })
    }
}
