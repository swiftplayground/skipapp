//
//  LoginViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController : UITableViewController {
    
    let loginService = LoginService()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let token = UserInformation().getUserToken() {
            performSegue(withIdentifier: "segue_to_stores", sender: nil)
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginService.delegate = self
    }
    
    @IBAction func signupPressed(_ sender: Any) {
        //check textfields
        //self.performSegue(withIdentifier: "segue_to_register", sender: nil)
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        //check textfields
        activityIndicator.startAnimating()
        
        let email = emailTextField.text as! String
        let password = passwordTextField.text as! String
        
        //loginService.requestLogin(payload: [("email","vcc3@me.com"),("password","7c4a8d09ca3762af61e59520943dc26494f8941b")])
        
        if !email.isEmpty && !password.isEmpty {
            loginService.requestLogin(payload: [("email",email),("password",password)])
        } else {
            showAlert(title: "Ops", message: "Something is wrong")
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    
    /* Segue Utils */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_to_stores" {
            if let stvc = segue.destination as? StoreTableViewController {
                stvc.navigationItem.hidesBackButton = true
            }
        }
    }
    
    /* Utils */
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension LoginViewController : LoginServiceProtocol {
    func didLoggedIn() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.performSegue(withIdentifier: "segue_to_stores", sender: nil)
        }
    }
    
    func didLoggedOut() {
        //
    }
    
    func didLoginUnauthorized(code: Int) {
        showAlert(title: "Ops..", message: "Authentication Failure")
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
}
