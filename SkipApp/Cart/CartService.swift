//
//  CartService.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

protocol CartServiceProtocol : class {
    func didCreateOrder()
    func didReceivedOrders(data: [[String:AnyObject]]?)
}

class CartService {
    
    weak var delegate: CartServiceProtocol?
    
    func requestOrderCreation(payload: [(String,Any)], orderItems: [(String,Any)]){
        GenericRestClient().createOrder(payload: payload, subdata: orderItems) { (data) in
            print(data)
        }
    }
    
    func requestOrders() {
        GenericRestClient().fetchOrders { (data) in
            self.delegate?.didReceivedOrders(data: data as? [[String:AnyObject]])
        }
    }

}
