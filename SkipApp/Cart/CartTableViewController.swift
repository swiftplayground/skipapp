//
//  CartTableViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class CartTableViewController: UITableViewController {
    
    let cartService = CartService()
    var cart = Cart().getCurrentCart()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartService.delegate = self
        print(cart)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath)
       
        let productName = (cart?[indexPath.item]["name"] as? String) ?? "Default Name"
        let productDescription = (cart?[indexPath.item]["description"] as? String) ?? "Default Description"
        let productPrice = (cart?[indexPath.item]["price"] as? Double) ?? -1.0
        
        if(productPrice == -1.0){
            cell.textLabel?.text = "\(productName)"
        } else {
            cell.textLabel?.text = "\(productName) - $\(productPrice)"
            cell.detailTextLabel?.text = productDescription
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = cart?[indexPath.item]
        print(product)
    }
    
    /* Nav Buttons */
    
    @IBAction func clearTapped(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "user_cart")
        prefs.synchronize()
        cart = []
        tableView.reloadData()
    }
    
    @IBAction func dealTapped(_ sender: Any) {
        
        var filter : [Any] = []
        
        for item in cart! {
            let itemId = item["id"] as? Int ?? 0
            filter.append([("productId",itemId),("quantity",1)])
        }
        
        let order : [(String, Any)] = [("deliveryAddress","Rua XYZ"),
                   ("contact", "81996140445"),
                   ("storeId", 2),
                   ("total", 27.5),
                   ("status", "Cart was requested")
        ]
        
        let orderItems : [(String, Any)] = [("productId", 6),("quantity", 1)]
        
        cartService.requestOrderCreation(payload: order, orderItems: orderItems)
    }
    
}

extension CartTableViewController : CartServiceProtocol {
    func didCreateOrder() {
        //
    }
    
    func didReceivedOrders(data: [[String : AnyObject]]?) {
        //
    }
}

