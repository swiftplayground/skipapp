//
//  Cart.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

class Cart {
    
    let preferences = UserDefaults.standard

    func getCurrentCart() -> [[String:AnyObject]]? {
        if let cart = preferences.object(forKey: "user_cart") as? [[String:AnyObject]]? {
            return cart
        }else{
            return []
        }
    }
    
    func appendToCart(product: [String:AnyObject]){
        if var cart = getCurrentCart() {
            cart.append(product)
            preferences.set(cart, forKey: "user_cart")
            preferences.synchronize()
            print(cart)
        } else {
            preferences.set([product], forKey: "user_cart")
            preferences.synchronize()
        }
    }
    

}
