//
//  UserInformations.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Security
import Foundation

class UserInformation {
    
    let preferences = UserDefaults.standard
    
    func getUserToken() -> String? {
        let preferences = UserDefaults.standard
        if let phone = preferences.object(forKey: "jwt_token") as? String {
            return phone
        }else{
            return nil
        }
    }
    
    func clearUserDefaults() {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "user_id")
        prefs.removeObject(forKey: "jwt_token")
        prefs.removeObject(forKey: "user_name")
        prefs.removeObject(forKey: "user_email")
        prefs.removeObject(forKey: "user_address")
        prefs.synchronize()
    }
}
