//
//  CousineService.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

protocol CousineServiceProtocol : class {
    func didReceivedCousines(data: [[String:AnyObject]]?)
}

class CousineService {
    
    weak var delegate : CousineServiceProtocol?

    func requestCousines(){
        GenericRestClient().fetchCousines(completion: { data in
            self.delegate?.didReceivedCousines(data: data as? [[String:AnyObject]])
        })
    }
}
