//
//  CousineCollectionViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class CousineCollectionCell : UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
}

class CousineCollectionViewController : UICollectionViewController {
    
    let cousineService = CousineService()
    var cousines: [[String : AnyObject]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cousineService.delegate = self
        cousineService.requestCousines()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cousines.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CousineCell", for: indexPath) as! CousineCollectionCell
        
        cell.backgroundColor = UIColor.white
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor;
        cell.layer.shadowOffset = CGSize(width:0.0, height:1.0);
        cell.layer.shadowOpacity = 1.0;
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath;
        
        cell.name.text = cousines[indexPath.item]["name"] as? String ?? "Default"
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cid = cousines[indexPath.item]["id"] as? Int {
            performSegue(withIdentifier: "segue_to_stores", sender: cid)
        }
    }
    
    /* Segue Utils */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_to_stores" {
            if let stvc = segue.destination as? StoreTableViewController {
                stvc.cousine = sender as? Int
            }
        }
        
    }

}

extension CousineCollectionViewController : CousineServiceProtocol {
    func didReceivedCousines(data: [[String : AnyObject]]?) {
        if let data = data {
            cousines = data
        }
        
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
}
