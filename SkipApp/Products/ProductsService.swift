//
//  ProductsService.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

protocol ProductsServiceProtocol : class {
    func didReceivedProducts(data: [[String:AnyObject]]?)
}

class ProductsService {
    
    weak var delegate : ProductsServiceProtocol?
    
    func requestProducts(){
        //
    }
    
    func requestProductsByStore(storeId: Int){
        GenericRestClient().fetchProductsByStore(storeId:storeId, completion: { data in
            self.delegate?.didReceivedProducts(data: data as? [[String:AnyObject]])
        })
    }

}
