//
//  StoreTableViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class ProductsTableViewController: UITableViewController {
    
    var store : Int?
    var products : [[String: AnyObject]] = []
    let productsService = ProductsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productsService.delegate = self
        
        if let sid = store as? Int {
            productsService.requestProductsByStore(storeId: sid)
        } else {
            productsService.requestProducts()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath)
        let productName = (products[indexPath.item]["name"] as? String) ?? "Default Address"
        let productDescription = (products[indexPath.item]["description"] as? String) ?? "Default Description"
        let productPrice = (products[indexPath.item]["price"] as? Double) ?? -1.0
        if( productPrice == -1.0 ){
            cell.textLabel?.text = "\(productName)"
        }else{
            cell.textLabel?.text = "\(productName) - $\(productPrice)"
        }
        cell.detailTextLabel?.text =  productDescription
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        let productName = product["name"] as? String ?? ""

        print(product)
        showAlert(title: "Add", message: "Do you like to add \(productName) on the cart?", action: true, product: product)
    }
    
    /* Utils */
    
    func showAlert(title: String, message: String, action: Bool, product: [String:AnyObject]?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if(action) {
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                self.addProductToCart(product: product)
            }))
            
            alert.addAction(UIAlertAction(title: "Not yet", style: UIAlertActionStyle.default, handler: nil))
            
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        }
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ProductsTableViewController {
    func addProductToCart(product: [String:AnyObject]?){
        if let prod = product {
            Cart().appendToCart(product: prod)
            self.showAlert(title: "YaY!", message: "Item added to the cart", action: false, product: nil)
        }
    }
}

extension ProductsTableViewController : ProductsServiceProtocol {
    func didReceivedProducts(data: [[String : AnyObject]]?) {
        if let payload = data as? [[String: AnyObject]] {
            products = payload
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

