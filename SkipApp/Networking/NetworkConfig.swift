//
//  NetworkConfig.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

enum NetworkConfig : String {
    case baseAPI = "http://api-vanhack-event-sp.azurewebsites.net/api/v1/"
}
