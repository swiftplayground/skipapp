//
//  GenericRestClient.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

class GenericRestClient {
    
    /* Start of Privates Methods */
    
    private func http(request: URLRequest, completion: @escaping (AnyObject) -> Void){
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            guard error == nil && data != nil else {
                print(error)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200 {
                completion(httpStatus.statusCode as AnyObject)
                return
            }
            
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                completion(json as AnyObject)
            } catch {
                completion(data! as AnyObject)
            }
            
        })
        task.resume()
    }
    
    private func request(method: String, contentType: String, url: String, payload:[(String,String)]?) -> URLRequest {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = method
        
        if let jwt = UserInformation().getUserToken(){
            request.addValue("Bearer \(jwt)", forHTTPHeaderField: "Authorization")
        }

        if(method == "POST"){
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
            var dictionary = [String: Any]()
            switch contentType {
            case "application/json":
                if let data = payload {
                    data.forEach {
                        dictionary[$0.0] = $0.1
                    }
                    let jsonData = try? JSONSerialization.data(withJSONObject: dictionary)
                    request.httpBody = jsonData
                }
            case "application/x-www-form-urlencoded":
                var postString = ""
                if let payload = payload {
                    for param in payload {
                        postString += "\(param.0)=\(param.1)&"
                    }
                    request.httpBody = postString.data(using: String.Encoding.utf8)
                }
            default:
                print("content type not allowed")
            }
        }
        
        return request as URLRequest
    }
    
    //hack
    private func request(method: String, contentType: String, url: String, payload:[(String,Any)]?, subdata: [(String,Any)]) -> URLRequest {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = method
        
        if(method == "POST"){
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
            if let jwt = UserInformation().getUserToken(){
                request.addValue("Bearer \(jwt)", forHTTPHeaderField: "Authorization")
            }
            var dictionary = [String: Any]()
            var subdictionary = [String: Any]()
            
            switch contentType {
            case "application/json":
                if let data = payload {
                    data.forEach {
                        dictionary[$0.0] = $0.1
                    }
                    
                    let subdata = subdata
                    subdata.forEach {
                        subdictionary[$0.0] = $0.1
                    }
                    
                    dictionary["orderItems"] = subdictionary
                    let jsonData = try? JSONSerialization.data(withJSONObject: dictionary)
                    request.httpBody = jsonData
                }
            case "application/x-www-form-urlencoded":
                var postString = ""
                if let payload = payload {
                    for param in payload {
                        postString += "\(param.0)=\(param.1)&"
                    }
                    request.httpBody = postString.data(using: String.Encoding.utf8)
                }
            default:
                print("content type not allowed")
            }
        }
        
        return request as URLRequest
    }
        
    private func post(path: String, contentType: String, payload:[(String,String)]) -> URLRequest{
        return self.request(method: "POST", contentType: contentType, url: NetworkConfig.baseAPI.rawValue + path, payload: payload)
    }
    
    //hack
    private func post(path: String, contentType: String, payload:[(String,Any)], subdata:[(String,Any)]) -> URLRequest{
        return self.request(method: "POST", contentType: contentType, url: NetworkConfig.baseAPI.rawValue + path, payload: payload, subdata: subdata )
    }
    
    private func get(path: String, contentType: String) -> URLRequest{
        return self.request(method: "GET", contentType: contentType, url: NetworkConfig.baseAPI.rawValue + path, payload: nil)
    }
    
    // API Authorization
    
    func login(payload: [(String,String)], completion: @escaping (AnyObject) -> Void){
        self.http(request: self.post(path: "Customer/auth", contentType: "application/x-www-form-urlencoded", payload: payload), completion: completion)
    }
    
    func register(payload: [(String,String)], completion: @escaping (AnyObject) -> Void){
        self.http(request: self.post(path: "Customer", contentType: "application/json", payload: payload), completion: completion)
    }
    
    // Cousines
    
    func fetchCousines(completion: @escaping (AnyObject) -> Void){
        self.http(request: self.get(path: "Cousine", contentType: ""), completion: completion)
    }
    
    // Stores
    
    func fetchStores(completion: @escaping (AnyObject) -> Void){
        self.http(request: self.get(path: "Store", contentType: ""), completion: completion)
    }
    
    func fetchStore(cousine:Int, completion: @escaping (AnyObject) -> Void){
        self.http(request: self.get(path: "Cousine/\(cousine)/stores", contentType: ""), completion: completion)
    }
    
    // Products
    
    func fetchProductsByStore(storeId:Int, completion: @escaping (AnyObject) -> Void){
        self.http(request: self.get(path: "Store/\(storeId)/products", contentType: ""), completion: completion)
    }
    
    // Cart
    
    func fetchOrders(completion: @escaping (AnyObject) -> Void){
        self.http(request: self.get(path: "Order/customer", contentType: ""), completion: completion)
    }
    
    func createOrder(payload: [(String,Any)], subdata:[(String,Any)], completion: @escaping (AnyObject) -> Void){
        self.http(request: self.post(path: "Order", contentType: "application/json", payload: payload, subdata: subdata ), completion: completion)
    }
   
}

