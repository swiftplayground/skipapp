//
//  NSMutableData.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
    
}
