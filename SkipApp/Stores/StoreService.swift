//
//  StoreService.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation

protocol StoreServiceProtocol : class {
    func didReceivedStores(data: [[String:AnyObject]]?)
}

class StoreService {
    
    weak var delegate : StoreServiceProtocol?
    
    func requestStores(){
        GenericRestClient().fetchStores(completion: { data in
            self.delegate?.didReceivedStores(data: data as? [[String:AnyObject]])
        })
    }
    
    func requestStoreByCousine(cousineId: Int){
        GenericRestClient().fetchStore(cousine: cousineId) { (data) in
            self.delegate?.didReceivedStores(data: data as? [[String:AnyObject]])
        }
    }
    
}
