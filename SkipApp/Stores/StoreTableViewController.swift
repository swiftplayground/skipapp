//
//  StoreTableViewController.swift
//  SkipApp
//
//  Created by Victor Casé on 17/03/18.
//  Copyright © 2018 Victor Casé. All rights reserved.
//

import Foundation
import UIKit

class StoreTableViewController: UITableViewController {
    
    var cousine : Int?
    var stores : [[String: AnyObject]] = []
    let storeService = StoreService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        storeService.delegate = self
        
        if let cid = cousine as? Int {
            storeService.requestStoreByCousine(cousineId: cid)
        } else {
            storeService.requestStores()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath)
        cell.textLabel?.text = stores[indexPath.item]["name"] as? String ?? "Default Address"
        cell.detailTextLabel?.text = stores[indexPath.item]["address"] as? String ?? "Default Address"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let sid = stores[indexPath.item]["id"] as? Int {
            performSegue(withIdentifier: "segue_to_products", sender: sid)
        }
    }
    
    @IBAction func openCart(_ sender: Any) {
        performSegue(withIdentifier: "segue_to_cart", sender: nil)
    }
    
    /* Segue Utils */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_to_products" {
            if let stvc = segue.destination as? ProductsTableViewController {
                stvc.store = sender as? Int
            }
        }
        
    }
}

extension StoreTableViewController : StoreServiceProtocol {
    func didReceivedStores(data: [[String : AnyObject]]?) {
        if let payload = data as? [[String: AnyObject]] {
            stores = payload
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
